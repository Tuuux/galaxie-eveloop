.PHONY: help install-python venv-create venv-requirements docs clean tests
SHELL=/bin/bash

#!make
include .env
export $(shell sed 's/=.*//' .env)

help:
	@echo  'CleaningUp:'
	@echo  '  clean               - Remove every created directory and restart from scratch'
	@echo  'VirtualEnv:'
	@echo  '  venv-create         - Create virtual env directory with python venv module'
	@echo  '  venv-requirements   - Call venv-create and install or update packages:'
	@echo  '                        pip, wheel, setuptools and install what is list on'
	@echo  '                        requirements.txt file'
	@echo  'Preparation:'
	@echo  '  prepare             - Easy way for make everything'
	@echo  'Tests:'
	@echo  '  tests               - Easy way for make tests over a CI'
	@echo  'Documentation:'
	@echo  '  docs                - Install Sphinx, then generate static html directory'

install-python:
	@ echo "INSTALL PYTHON REQUIREMENTS"
	apt install -y python3 python3-pip python3-venv

venv-create:
	@ echo "CONTROL VENV"
	@ test -d $(DEV_VENV_DIRECTORY) || cd $(DEV_DIRECTORY) && python3 -m venv $(DEV_VENV_NAME)

venv-requirements: venv-create
	@ echo "INSTALL VENV REQUIREMENTS"
	@ ${DEV_VENV_ACTIVATE} &&\
	pip install -U --no-cache-dir -q pip setuptools wheel &&\
	python setup.py develop

clean:
	@ echo "CLEANING UP"
	rm -rf ./venv
	rm -rf ~/.cache/pip
	rm -rf ./docs/source/glxeveloop.*.rst
	rm -rf ./docs/source/glxeveloop.rst
	rm -rf ./.eggs
	rm -rf ./glxeveloop/__pycache__
	rm -rf ./glxeveloop/*.pyc

prepare: venv-requirements

docs: venv
	@ echo "BUILD DOCUMENTATIONS"
	@ ${DEV_VENV_ACTIVATE} &&\
	pip3 install --upgrade pip setuptools wheel --no-cache-dir --quiet &&\
  	pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
	cd $(DEV_DIRECTORY)/docs &&\
	sphinx-apidoc -e -f -o source/ ../glxeveloop/ &&\
	make html

tests:
	@ echo "EXECUTE TESTS"
	@ ${DEV_VENV_ACTIVATE} && python setup.py green -r