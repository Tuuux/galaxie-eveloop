#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: the Galaxie EveLoop Team, all rights reserved

from glxeveloop import MainLoop
from time import time

count = 1
mainloop = MainLoop().loop
mainloop.timer.fps.min = 30
mainloop.timer.fps.value = 60
mainloop.timer.fps.max = 30
start = time()


def pre():
    global count
    global mainloop
    global start
    if count >= 30:
        mainloop.stop()


def cmd():
    global count
    global mainloop

    print("COUNT: {0}, CPS: {1}".format(count, mainloop.timer.fps.value))


def post():
    global count
    count += 1
    if count > 30:
        print("TIME: {0}".format((time() - start)))
        print("PRECISION: {0}".format(1 - (time() - start)))


def main():
    mainloop.hooks.cmd = cmd
    mainloop.hooks.pre = pre
    mainloop.hooks.post = post

    mainloop.start()


if __name__ == '__main__':
    main()
