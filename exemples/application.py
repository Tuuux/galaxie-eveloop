from glxeveloop.loop import Loop
from random import randint
from time import sleep


class Application(object):
    def __init__(self):
        self.count = 1

    def cmd(self):
        sleep(randint(1, 5) / 100)
        if Loop().timer.queue.buffer[0]:
            print("COUNT:{0} CPS:{1} TIME:{2} sec".format(
                self.count, Loop().timer.fps.value, 1 / Loop().timer.fps.value
            ))
        else:
            print("COUNT:{0} CPS:{1} TIME:unknown".format(
                self.count, Loop().timer.fps.value
            ))

    def pre(self):
        if self.count >= 1000:
            Loop().stop()

    def post(self):
        self.count += 1


def main():
    mainloop = Loop()
    app = Application()

    mainloop.timer.queue.size = 15
    mainloop.timer.fps.min = 1.0
    # mainloop.timer.fps.max = 25.0
    # mainloop.timer.fps.value = 20.0
    mainloop.hooks.cmd = app.cmd
    mainloop.hooks.pre = app.pre
    mainloop.hooks.post = app.post
    mainloop.start()


if __name__ == '__main__':
    main()
