==================
GALAXIE GLXEVELOOP
==================

CHANGELOGS
----------
**0.1.4 - 20210213**
  * Introduce Builder Pattern
  * MainLoop is make by Builder
  * MainLoop use Singleton Pattern
  * Replace EventList Buffer by queue.Queue()
  * Replace Timer Memory by by queue.Queue()
  * Big refactoring

**0.1.3 - 20210202**
  * Code review
  * Add Timer Class
  * Rename many class
  * 100% code cover

**0.1.2 - 20210202**
  * Refactoring
  * Work with GLXCurses

**0.1.1 - 20210131**
  * Add unittest

**0.1 - 20210131**
  * Fork from GLXCurses MainLoop
  * Black reformat