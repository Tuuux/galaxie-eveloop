=============================
Galaxie EveLoop documentation
=============================
.. figure::  images/logo_galaxie.png
   :align:   center

Description
-----------
Galaxie Event Loop is a low tech main loop couple with a event bus.

After many years as kernel of Galaxie Curses project, it have been as decision to extract the main loop and the event bus.
Especially that because it work and be very small.


Links
-----
 * GitLab: https://gitlab.com/Tuuux/galaxie-eveloop/
 * Read the Doc: https://galaxie-eveloop.readthedocs.io/
 * PyPI: https://pypi.org/project/galaxie-eveloop/
 * PuPI Test: https://test.pypi.org/project/galaxie-eveloop/

Contents
--------

.. toctree::
   :maxdepth: 4

   modules
   Basic-Types

Installation via pip
--------------------
Pypi

.. code:: bash

  pip install galaxie-eveloop

Pypi Test

.. code:: bash

  pip install -i https://test.pypi.org/simple/ galaxie-eveloop

License
-------

.. toctree::
   :maxdepth: 0

   LICENSE

See the LICENCE_

.. _LICENCE: https://gitlab.com/Tuuux/galaxie-eveloop/blob/master/LICENSE.rst

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
